---
title: 计划
date: 2021-01-04 11:36:30
---

# 2021

编程:

- [ ] [C++ Core Guidelines](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)

  <div class="progress" width="%20">
    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width=0%"></div>
  </div>

- [ ] C++ Concurrency in Action

  <div class="progress" width="%20">
    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width=0%"></div>
  </div>


算法:

- [ ] 数字图形处理

  <div class="progress" width="%20">
    <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%"></div>
  </div>


- [ ] 图像工程

  <div class="progress" width="%20">
    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width=0%"></div>
  </div>
  
- [ ] [视觉SLAM](https://helywin.github.io/2021/11/18/%E8%A7%86%E8%A7%89SLAM/)

  <div class="progress" width="%20">
    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width=50%"></div>
  </div>


哲学:

- [ ] 纯粹理性批判

  <div class="progress" width="%20">
    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width=1%"></div>
  </div>
  
  

[先验要素论笔记](https://gitmind.cn/app/doc/c2dfc8ce71e67f55a4d5967b0041c779)