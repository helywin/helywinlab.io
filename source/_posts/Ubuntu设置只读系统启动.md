---
title: Ubuntu设置只读系统启动
author: helywin
date: 2021-11-27 10:54:52
updated: 2021-11-27 10:54:52
category: Linux
tags: 
  - overlayroot
  - Ubuntu
excerpt: 保护文件系统不被破坏
---

# 介绍

因为工作需要，工控机在遇到突发情况断电时会导致根文件系统破坏，启动的时候就直接进入修复系统的命令行，导致无法远程进行修复或者解决问题。

采用程序和系统分开分区的方式，用overlayroot保护系统分区不被修改，可以解决系统在断电后不能启动的问题（但是程序分区如果有写文件依旧可能会被破坏）

# 安装方法

## 安装

```shell
sudo apt install overlay
```

修改配置

```shell
sudo vim /etc/overlayroot.conf
```

把

```
overlayroot=""
```

修改为

```
overlayroot="tmpfs:swap=1,recurse=0"
```

重启后用`mount`命令查看根文件系统是不是以`tmpfs-root`挂载

## grub配置

默认系统是只读启动，所以要进入可写模式才能修改配置文件生效，在grub引导菜单按上下键，根据提示修改默认的grub，在第一个`menuentry`大括号里面的`linux /vmlinuz-x.x.x`一行末尾添加`overlayroot=disabled`，前面有空格，用该配置启动就能进入可写的系统

修改grub配置

```
sudo vim /boot/grub/grub.cfg
```

在里面复制第一个`menuentry`，改名为`Ubuntu-rw`，按照之前那样修改

```
menuentry 'Ubuntu-rw' --class ubuntu --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple- fdf23965-681d-4212-8286-602042940fec' {
     recordfail
     load_video
     gfxmode $linux_gfx_mode
     insmod gzio
     if [ x$grub_platform = xxen ]; then insmod xzio; insmod lzopio; fi
     insmod part_gpt
     insmod ext2
     if [ x$feature_platform_search_hint = xy ]; then
       search --no-floppy --fs-uuid --set=root  a2deb8bc-0138-4918-9a20-a2d05ed17714
     else
       search --no-floppy --fs-uuid --set=root a2deb8bc-0138-4918-9a20-a2d05ed17714
     fi
         linux   /vmlinuz-5.4.0-89-generic root=UUID=fdf23965-681d-4212-8286-602042940fec ro  quiet splash         $vt_handoff overlayroot=disabled 
     initrd  /initrd.img-5.4.0-89-generic
}
menuentry 'Ubuntu' --class ubuntu --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple- fdf23965-681d-4212-8286-602042940fec' {
     recordfail
     load_video
     gfxmode $linux_gfx_mode
     insmod gzio
     if [ x$grub_platform = xxen ]; then insmod xzio; insmod lzopio; fi
     insmod part_gpt
     insmod ext2
     if [ x$feature_platform_search_hint = xy ]; then
       search --no-floppy --fs-uuid --set=root  a2deb8bc-0138-4918-9a20-a2d05ed17714
     else
       search --no-floppy --fs-uuid --set=root a2deb8bc-0138-4918-9a20-a2d05ed17714
     fi
         linux   /vmlinuz-5.4.0-89-generic root=UUID=fdf23965-681d-4212-8286-602042940fec ro  quiet splash         $vt_handoff
     initrd  /initrd.img-5.4.0-89-generic
 }
```

放在前面的一条会默认启动

