---
title: ROS线程XmlRpc::XmlRpcDispatch::work函数CPU占用过高问题
date: 2021-12-9 9:59:30
updated: 2021-12-9 9:59:30
tags:
  - ROS
category: ROS
excerpt: ROS的NodeHandle使用问题
index_img: https://s3.ax1x.com/2020/12/27/r5aVTs.png
---

## 现象

线程中有一个由ROS自己创建的线程CPU占用率接近100%，该线程中的`XmlRpc::XmlRpcDispatch::work()`函数占用过高，使用`sudo perf top -p [pid]`得到如图

<img src="https://s1.ax1x.com/2021/12/09/oWTY1f.png" width=600 />

该程序运行一开始占用率很低，几分钟后该函数占用达到4.8%，而后突然变成100%

使用htop的ptrace查看该线程的系统调用，发现以很高的频率在调用POLLIN

<img src="https://s1.ax1x.com/2021/12/09/oWTang.png" width=600/>

/proc/[pid]/fd下面显示fd=27的IO为socket

## 解决办法

最后检查代码发现代码中有多出创建了`ros::NodeHandle`，导致出现了问题，传入一个`ros::NodeHandle`后问题得到解决

