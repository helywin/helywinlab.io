# 说明

<img alt="GitHub Workflow Status" src="https://img.shields.io/github/workflow/status/helywin/helywin.github.io/Deploy" />

个人博客，不定期更新内容，个人邮箱[地址](mailto:jiang770882022@hotmail.com)，微信同号

博客框架：hexo

主题：fluid

图床：imgtu

发布：Github Actions

## 备注

更新所有包命令:

```
 npm-check -u
 yarn upgrade-interactive --latest
```

使用了修改过的`kramed`和`hexo-renderer-kramed`包，以便内联公式中的下划线不会被解释为斜体

输入公式时检查`{{` `{#` `}}`等字符串是否存在，对标签系统解析会造成影响，可以通过加空格解决

